# Assemblers Almanac

This document describes the resources the regulars at the Assemblers Community Discord sever like to recommend. There are lots of different ways to learn, and everyone has their own preferences--so any number of references are given on each topic to supply diversity. And also depth.

There are resources here about learning assembly language on different platforms, but also many related subjects, since assembly language programmers generally like to learn things from the bottom up.

### [Discord](https://discord.gg/fxqBhgasTN)

## Table of Contents
- [8080, z80](#80)
- [x86](#x86)
- [ia64](#x86)
- [MIPS](#x86)
- [ARM](#arm)
- [OSDev](#osdev)
- [Other](#other)

<a name="80"></a>
## 8080/z80
- [XLT86 8080 to x86 translator](http://www.s100computers.com/Software%20Folder/Assembler%20Collection/Digital%20Research%20XLT86%20Manual.pdf)
<a name="x86"></a>
## x86
- [Tiny Guide to x86 Assembly](https://cs.dartmouth.edu/~sergey/cs258/tiny-guide-to-x86-assembly.pdf)
## Itanium
<a name="ia64"></a>
- [New Old Thing - Itanium](https://devblogs.microsoft.com/oldnewthing/page/2?s=itanium)
## OSDev
<a name="osdev"></a>
- [AMD64 Programming Manual - Vol2. Systems Programming](https://www.amd.com/system/files/TechDocs/24593.pdf)
- [*Operating Sytems: Three Easy Pieces*](https://www.amazon.com/Operating-Systems-Three-Easy-Pieces/dp/198508659X/) This book treats operating systems in three "easy" pieces: virtualization, concurrency, and persistence. Also downloadable from [ostep.org](http://www.ostep.org/). The chapters are all only a dozen or so pages long, and easily digested. The diagrams could be better-drawn, and examples often don't build on each other and instead change context and assumptions from one to the next.
- [Operating Systems Concepts](https://www.amazon.com/gp/product/B07CVKH7BD/) by Abraham Silberschatz. Older versions of this book are great. Once regarded one of the best OS textbooks available, the newest 10th edition is hampered by the recent trend to offer goofy pacakging for text books: online only, online-bundled codes, and loose-leaf bindings for three-ring binders. The treatment of scheduling and memory management are unparalleled.
- [Modern Operating Systems](https://www.amazon.com/Modern-Operating-Systems-Andrew-Tanenbaum/dp/013359162X) by Andrew Tanenbaum and Herbert Bos. The 4th edition of this book revises this seminal work for newer concurrency and caching models. Another top-tier textbook, the deep treatment of design and implementation tradeoffs is well-regarded.

<a name="other"></a>
